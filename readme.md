# ABEUNID Server Side
Repositório do servidor ABEUNID

Este servidor utiliza de GraphQL para gravar e pesquisar dados dos nossos usuários.

### Requisitos
- NPM
- Docker
- MongoDB

### Execução em desenvolvimento
1. Subir a imagem docker do MongoDB
`docker run -d -p 27017-27019:27017-27019 --name mongodb mongo:latest` 

2. Adicionar as variáveis de ambiente

3. Compilar o servidor
`npm run build`

4. Iniciar o servidor com hot reload
`npm run dev`

### Execução em produção
1. Garantir que o MongoDB esteja rodando

2. Adicionar as variáveis de ambiente

3. Iniciar o servidor
`npm run start`

### Referências adicionais
[Tutorial sobre GraphQL com TypeScript](https://hashnode.com/post/building-a-nodejs-api-with-typescript-and-graphql-cjrrojjx200uqrxs1ngtitx9p)
