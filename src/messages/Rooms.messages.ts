import { Room } from "@/model/room/Room.model";

export enum RoomsMessageType {
  CREATED,
  UPDATED,
  DELETED,
}

export interface IRoomsMessage {
  type: RoomsMessageType;
  room?: Room;
}

export const IRoomsMessageTopic = "ROOMS";
