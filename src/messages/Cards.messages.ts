import { Card } from "@/model/card/Card.model";

export enum CardsMessageType {
  CREATED,
  UPDATED,
  DELETED,
}

export interface ICardsMessage {
  type: CardsMessageType;
  card?: Card;
}

export const ICardsMessageTopic = "CARDS";
