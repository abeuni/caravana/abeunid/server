import { Person } from "@/model/person/Person.model";

export enum PersonsMessageType {
  CREATED,
  UPDATED,
  DELETED,
}

export interface IPersonsMessage {
  type: PersonsMessageType;
  person?: Person;
}

export const IPersonsMessageTopic = "PERSONS";
