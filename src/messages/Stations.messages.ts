import { Station } from "@/model/station/Station.model";

export enum StationsMessageType {
  CREATED,
  UPDATED,
  DELETED,
}

export interface IStationsMessage {
  type: StationsMessageType;
  station?: Station;
}

export const IStationsMessageTopic = "STATIONS";
