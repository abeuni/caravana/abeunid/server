import { registerEnumType } from "type-graphql";

export enum CivilStatus {
  SINGLE = "single",
  MARRIED = "married",
  STABLE_UNION = "stable_union",
  SEPARATED = "separated",
  DIVORCED = "divorced",
  WIDOWED = "widowed",
}

registerEnumType(CivilStatus, {
  description: "Represents Civil Status (eg. single, married",
  name: "CivilStatus",
});
