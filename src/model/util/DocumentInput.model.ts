import { Field, InputType } from "type-graphql";
import { DocumentType } from "./Document.model";

@InputType({ description: "Represents a document input" })
export class DocumentInput {
  @Field({ description: "Document's type" })
  public type: DocumentType;

  @Field({ description: "Document's value" })
  public value: string;
}
