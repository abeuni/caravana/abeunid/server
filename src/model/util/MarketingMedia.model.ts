import { registerEnumType } from "type-graphql";

export enum MarketingMedia {
  POSTER = "POSTER",
  PAMPHLET_STREET = "PAMPHLET_STREET",
  PAMPHLET_HOME = "PAMPHLET_HOME",
  FACEBOOK = "FACEBOOK",
  INSTAGRAM = "INSTAGRAM",
  WHATSAPP = "WHATSAPP",
  ACQUAINTANCE_VERBAL = "ACQUAINTANCE_VERBAL",
  CEU_MARKETING = "CEU_MARKETING",
  OTHER = "OTHER",
}

registerEnumType(MarketingMedia, {
  description: "Represents where the person found about the event",
  name: "MarketingMedia",
});
