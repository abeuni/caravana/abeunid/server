import { Field, ObjectType, registerEnumType } from "type-graphql";

export enum DocumentType {
  CPF = "CPF",
  CNH = "CNH",
  RG = "RG",
  SUS = "SUS",
  Other = "Other",
}

registerEnumType(DocumentType, {
  description: "Represents a Document Type",
  name: "DocumentType",
});

@ObjectType({ description: "Represents a document" })
export class Document {
  @Field({ description: "Document's type" })
  public type: DocumentType;

  @Field({ description: "Document's value" })
  public value: string;
}
