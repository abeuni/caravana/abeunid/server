import { registerEnumType } from "type-graphql";

export enum BiologicalSex {
  M = "male",
  F = "female",
}

registerEnumType(BiologicalSex, {
  description: "Represents Biological Sex",
  name: "BiologicalSex",
});
