import { Field, InputType, ObjectType } from "type-graphql";
import { CEPScalar } from "./CEP.model";

@ObjectType({ description: "Input for address updating" })
export class AddressUpdate {
  @Field((type) => CEPScalar, { description: "CEP number", nullable: true })
  public cep?: string;

  @Field({ description: "Address complement", nullable: true })
  public complement?: string;

  @Field({ description: "House Number", nullable: true })
  public number?: string;

  @Field({ description: "Street (If CEP not provided)", nullable: true })
  public street?: string;
}
