import { Field, ObjectType } from "type-graphql";
import { CEPScalar } from "./CEP.model";

@ObjectType({ description: "Represents a search result for a CEP search" })
export class CEPSearchResult {
  @Field((type) => CEPScalar, { description: "CEP number" })
  public cep: string;

  @Field({ description: "Street (If CEP not provided)" })
  public street: string;

  @Field({ description: "Neighborhood" })
  public neighborhood: string;
}
