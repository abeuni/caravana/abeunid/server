import { Field, InputType, ObjectType } from "type-graphql";

@ObjectType({ description: "Represents contact information" })
export class ContactInformationUpdate {
  @Field({ description: "Contact Phone Number", nullable: true })
  public phoneNumber?: string;

  @Field({ description: "Emergency Contact Phone Number", nullable: true })
  public emergencyPhoneNumber?: string;

  @Field({ description: "Emergency Contact Name", nullable: true })
  public emergencyContactName?: string;
}
