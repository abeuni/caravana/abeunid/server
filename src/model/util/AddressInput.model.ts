import { Field, InputType } from "type-graphql";
import { CEPScalar } from "./CEP.model";

@InputType({ description: "Input for address creation" })
export class AddressInput {
  @Field((type) => CEPScalar, { description: "CEP number", nullable: true })
  public cep?: string;

  @Field({ description: "Address complement", nullable: true })
  public complement?: string;

  @Field({ description: "House Number" })
  public number: string;

  @Field({ description: "Street (If CEP not provided)", nullable: true })
  public street?: string;
}
