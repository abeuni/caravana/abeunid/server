import { ApolloError } from "apollo-server";
import { GraphQLScalarType, Kind } from "graphql";

export const CEPScalar = new GraphQLScalarType({
  description: "Represents a CEP number",
  name: "CEP",
  parseValue(value: string): string {
    if (!/^[0-9]{5}\-[0-9]{3}$/.test(value)) {
      throw new ApolloError("Invalid CEP value", "400");
    }

    return value;
  },
  serialize(value: string): string {
    if (!/^[0-9]{5}\-[0-9]{3}$/.test(value)) {
      throw new ApolloError("Invalid CEP value", "500");
    }

    return value;
  },
  parseLiteral(ast) {
    if (ast.kind !== Kind.STRING || !/^[0-9]{5}\-[0-9]{3}$/.test(ast.value)) {
      throw new ApolloError("Invalid CEP value", "400");
    }

    return ast.value;
  },
});
