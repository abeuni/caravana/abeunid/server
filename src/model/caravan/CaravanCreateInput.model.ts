import { Field, InputType } from "type-graphql";

@InputType({ description: "Data for creating a Caravan" })
export class CaravanCreateInput {
  @Field({ description: "Caravan Name" })
  public name: string;

  @Field({ description: "Start Date" })
  public start: Date;

  @Field({ description: "End Date" })
  public end: Date;
}
