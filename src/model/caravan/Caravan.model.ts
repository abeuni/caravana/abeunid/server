import { Indexable } from "@/model/Indexable";

import { Field, ObjectType } from "type-graphql";

@ObjectType({ description: "Represents a Caravan" })
export class Caravan extends Indexable<Caravan> {
  @Field({ description: "Caravan Name" })
  public name: string;

  @Field({ description: "Start Date" })
  public start: Date;

  @Field({ description: "End Date" })
  public end: Date;

  // Required
  public getCollection() {
    return "caravans";
  }
}
