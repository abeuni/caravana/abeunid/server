import { Indexable } from "@/model/Indexable";
import { BiologicalSex } from "@/model/util/BiologicalSex.model";

import { Field, ObjectType } from "type-graphql";

import { getDB } from "@/dataloader/db";
import { PhysicalFormulary } from "@/model/formulary/PhysicalFormulary.model";
import { Address } from "@/model/util/Address.model";
import { CivilStatus } from "@/model/util/CivilStatus.model";
import { ContactInformation } from "@/model/util/Contact.model";
import { Event } from "../event/Event.model";
import { Document } from "../util/Document.model";
import { MarketingMedia } from "../util/MarketingMedia.model";

@ObjectType({ description: "Represents a Person" })
export class Person extends Indexable<Person> {
  @Field({ description: "Person Full Name" })
  public fullname: string;

  @Field({ description: "Person Birth Date" })
  public birthDate: Date;

  @Field((type) => BiologicalSex, { description: "Person Biological Sex" })
  public sex: BiologicalSex;

  @Field((type) => Document, { description: "Person's identifiying document", nullable: true })
  public document?: Document;

  @Field({ description: "Person Image Use Permission" })
  public imagePermission: boolean;

  @Field((type) => Address, {
    description: "Person House Address",
    nullable: true,
  })
  public address?: Address;

  @Field((type) => CivilStatus, { description: "Person's civil status" })
  public civilStatus: CivilStatus;

  @Field((type) => ContactInformation, {
    description: "Person's contact information",
    nullable: true,
  })
  public contact?: ContactInformation;

  @Field((type) => [PhysicalFormulary], {
    description: "Physical formularies related to this person",
  })
  public formularies: PhysicalFormulary[];

  @Field((type) => [Event], {
    description: "Events",
  })
  public events: Event[];

  @Field((type) => MarketingMedia, {
    description: "Marketing Media used to dicover event",
    nullable: true,
  })
  public marketingMedia?: MarketingMedia;

  @Field({
    description: "Observations",
    nullable: true,
  })
  public observations: string;

  public getCollection() {
    return "persons";
  }
}

getDB().then((db) => db.collection("persons").createIndex({
  "document.value": "text",
  "fullname": "text",
}));
