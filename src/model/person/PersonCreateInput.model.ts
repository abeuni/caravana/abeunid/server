import { AddressInput } from "@/model/util/AddressInput.model";
import { BiologicalSex } from "@/model/util/BiologicalSex.model";
import { CivilStatus } from "@/model/util/CivilStatus.model";
import { ContactInformationInput } from "@/model/util/ContactInput.model";
import { Field, InputType } from "type-graphql";
import { DocumentInput } from "../util/DocumentInput.model";
import { MarketingMedia } from "../util/MarketingMedia.model";

@InputType({ description: "Data for creating a Person" })
export class PersonCreateInput {
  @Field({ description: "Person Full Name" })
  public fullname: string;

  @Field({ description: "Person Birth Date" })
  public birthDate: Date;

  @Field((type) => BiologicalSex, { description: "Person Biological Sex" })
  public sex: BiologicalSex;

  @Field((type) => DocumentInput, {
    description: "Person's identifiying document",
    nullable: true,
  })
  public document?: DocumentInput;

  @Field({ description: "Person Image Use Permission" })
  public imagePermission: boolean;

  @Field((type) => AddressInput, {
    description: "Person House Address",
    nullable: true,
  })
  public address?: AddressInput;

  @Field((type) => CivilStatus, { description: "Person's civil status" })
  public civilStatus: CivilStatus;

  @Field((type) => ContactInformationInput, {
    description: "Person's contact information",
    nullable: true,
  })
  public contact?: ContactInformationInput;

  @Field((type) => MarketingMedia, {
    description: "Marketing Media used to discover event",
  })
  public marketingMedia: MarketingMedia;

  @Field({
    description: "Observations",
    nullable: true,
  })
  public observations?: string;
}
