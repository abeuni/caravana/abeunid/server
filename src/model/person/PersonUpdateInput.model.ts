
import { BiologicalSex } from "@/model/util/BiologicalSex.model";
import { CivilStatus } from "@/model/util/CivilStatus.model";
import { Field, InputType } from "type-graphql";
import { AddressUpdateInput } from "../util/AddressUpdateInput.model";
import { ContactInformationUpdateInput } from "../util/ContactUpdateInput.model";
import { DocumentInput } from "../util/DocumentInput.model";
import { MarketingMedia } from "../util/MarketingMedia.model";

@InputType({ description: "Data for updating a Person" })
export class PersonUpdateInput {
  @Field({ description: "Person Full Name", nullable: true })
  public fullname?: string;

  @Field({ description: "Person Birth Date", nullable: true })
  public birthDate?: Date;

  @Field((type) => BiologicalSex, { description: "Person Biological Sex", nullable: true })
  public sex?: BiologicalSex;

  @Field((type) => DocumentInput, {
    description: "Person's identifiying document",
    nullable: true,
  })
  public document?: DocumentInput;

  @Field({ description: "Person Image Use Permission", nullable: true })
  public imagePermission?: boolean;

  @Field((type) => AddressUpdateInput, {
    description: "Person House Address",
    nullable: true,
  })
  public address?: AddressUpdateInput;

  @Field((type) => CivilStatus, {
    description: "Person's civil status",
    nullable: true,
  })
  public civilStatus?: CivilStatus;

  @Field((type) => ContactInformationUpdateInput, {
    description: "Person's contact information",
    nullable: true,
  })
  public contact?: ContactInformationUpdateInput;

  @Field((type) => MarketingMedia, {
    description: "Marketing Media used to dicover event",
    nullable: true,
  })
  public marketingMedia?: MarketingMedia;

  @Field({
    description: "Observations",
    nullable: true,
  })
  public observations?: string;
}
