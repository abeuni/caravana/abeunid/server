
import { BiologicalSex } from "@/model/util/BiologicalSex.model";
import { CivilStatus } from "@/model/util/CivilStatus.model";
import { Field, ObjectType } from "type-graphql";
import { AddressUpdate } from "../util/AddressUpdate.model";
import { ContactInformationUpdate } from "../util/ContactUpdate.model";
import { Document } from "../util/Document.model";
import { MarketingMedia } from "../util/MarketingMedia.model";

@ObjectType({ description: "Data for updating a Person" })
export class PersonUpdateChanges {
  @Field({ description: "Person Full Name", nullable: true })
  public fullname?: string;

  @Field({ description: "Person Birth Date", nullable: true })
  public birthDate?: Date;

  @Field((type) => BiologicalSex, { description: "Person Biological Sex", nullable: true })
  public sex?: BiologicalSex;

  @Field((type) => Document, {
    description: "Person's identifiying document",
    nullable: true,
  })
  public document?: Document;

  @Field({ description: "Person Image Use Permission" })
  public imagePermission: boolean;

  @Field((type) => AddressUpdate, {
    description: "Person House Address",
    nullable: true,
  })
  public address?: AddressUpdate;

  @Field((type) => CivilStatus, { description: "Person's civil status" })
  public civilStatus: CivilStatus;

  @Field((type) => ContactInformationUpdate, {
    description: "Person's contact information",
    nullable: true,
  })
  public contact?: ContactInformationUpdate;

  @Field((type) => MarketingMedia, {
    description: "Marketing Media used to dicover event",
    nullable: true,
  })
  public marketingMedia?: MarketingMedia;

  @Field({
    description: "Observations",
    nullable: true,
  })
  public observations?: string;
}
