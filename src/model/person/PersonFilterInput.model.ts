import { Field, InputType } from "type-graphql";

@InputType({ description: "Data for creating a Person" })
export class PersonFilterInput {
  @Field({ description: "Search String", nullable: true })
  public search?: string;
}
