import { Ctx, FieldResolver, Resolver, Root } from "type-graphql";

import { IRequestContext } from "@/middleware/context";
import { Person } from "@/model/person/Person.model";
import { Event, EventEntityType } from "../event/Event.model";
import { PhysicalFormulary } from "../formulary/PhysicalFormulary.model";

@Resolver((of) => Person)
export class PersonResolver {
  @FieldResolver()
  public async formularies(@Ctx() ctx: IRequestContext, @Root() person: Person) {
    return PhysicalFormulary.find(ctx, {
      personId: person.id,
    });
  }

  @FieldResolver()
  public async events(@Ctx() ctx: IRequestContext, @Root() person: Person) {
    return Event.find(ctx, {
      relevant: person.id,
    }, (cursor) => cursor.sort({ created: 1 }));
  }
}
