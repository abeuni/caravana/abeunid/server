import { Field, InputType } from "type-graphql";

@InputType({ description: "Data for creating a Room" })
export class RoomCreateInput {
  @Field({ description: "Room Name" })
  public name: string;

  @Field({ description: "Caravan ID" })
  public caravanId: string;
}
