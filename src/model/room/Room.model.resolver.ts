import { Ctx, FieldResolver, Resolver, Root } from "type-graphql";

import { IRequestContext } from "@/middleware/context";
import { Room } from "@/model/room/Room.model";
import { Caravan } from "../caravan/Caravan.model";

@Resolver((of) => Room)
export class RoomResolver {
  @FieldResolver()
  public async caravan(@Ctx() ctx: IRequestContext, @Root() room: Room) {
    return Caravan.load(ctx, room.caravanId);
  }
}
