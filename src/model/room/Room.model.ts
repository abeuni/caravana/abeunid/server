import { Indexable } from "@/model/Indexable";

import { Field, ObjectType } from "type-graphql";

import { Caravan } from "../caravan/Caravan.model";

@ObjectType({ description: "Represents a Physical Formulary (Set of pages)" })
export class Room extends Indexable<Room> {
  @Field({ description: "Room Name" })
  public name: string;

  @Field((type) => Caravan, { description: "Caravan" })
  public caravan: Caravan;
  public caravanId: string;

  public getCollection() {
    return "rooms";
  }
}
