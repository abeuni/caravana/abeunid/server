import { Indexable } from "@/model/Indexable";

import { Field, ObjectType } from "type-graphql";

@ObjectType({ description: "Represents a Physical Formulary (Set of pages)" })
export class FormularyPage extends Indexable<FormularyPage> {
  @Field({ description: "QR Code" })
  public code: string;

  @Field((type) => [String], { description: "Photos of the Page" })
  public photos: string[];
  public photoIds: string[];

  public formularyId: string;

  public getCollection() {
    return "formulary_pages";
  }
}
