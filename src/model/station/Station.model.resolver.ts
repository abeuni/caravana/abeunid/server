import { Ctx, FieldResolver, Resolver, Root } from "type-graphql";

import { IRequestContext } from "@/middleware/context";
import { Station } from "@/model/station/Station.model";
import { Event } from "../event/Event.model";
import { Room } from "../room/Room.model";

@Resolver((of) => Station)
export class StationResolver {
  @FieldResolver()
  public async room(@Ctx() ctx: IRequestContext, @Root() station: Station) {
    return station.roomId && Room.load(ctx, station.roomId);
  }

  @FieldResolver()
  public async events(@Ctx() ctx: IRequestContext, @Root() station: Station) {
    return Event.find(ctx, {
      relevant: station.id,
    });
  }
}
