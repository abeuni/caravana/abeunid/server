import { Indexable } from "@/model/Indexable";

import { Field, Int, ObjectType } from "type-graphql";

import { Event } from "../event/Event.model";
import { Room } from "../room/Room.model";

@ObjectType({ description: "Represents a Physical Formulary (Set of pages)" })
export class Station extends Indexable<Station> {
  @Field({ description: "Station Name" })
  public name: string;

  @Field((type) => Int, { description: "Station Mesh Node ID" })
  public node: number;

  @Field((type) => Room, { description: "The room this station is currently at", nullable: true })
  public room?: Room;
  public roomId?: string;

  @Field((type) => [Event], {
    description: "Events",
  })
  public events: Event[];

  public getCollection() {
    return "stations";
  }
}
