import { Indexable } from "@/model/Indexable";

import { Field, ObjectType } from "type-graphql";
import { Event, EventEntityType, EventType } from "../event/Event.model";

import { IRequestContext } from "@/middleware/context";
import { Person } from "../person/Person.model";
import { ApolloError } from 'apollo-server';

@ObjectType({ description: "Represents a Physical Formulary (Set of pages)" })
export class Card extends Indexable<Card> {
  @Field({ description: "QR Code" })
  public code: string;

  @Field((type) => [Event], {
    description: "Events",
  })
  public events: Event[];

  @Field((type) => Person, {
    description: "Currently assigned person",
    nullable: true,
  })
  public person?: Person;

  public async getPerson (ctx: IRequestContext, cardId: string) {
    const events = await Event.find(ctx, {
      "data.cardId": cardId,
      "entityType": EventEntityType.PERSON,
      "type": EventType.ASSOCIATED,
    });

    if (!events || events.length <= 0)
      return null

    const lastEvent = events[events.length - 1]

    if (lastEvent instanceof Error)
      throw new ApolloError('Unknown Error')

    return Person.load(ctx, lastEvent.entityId)
  }

  // Required
  public getCollection () {
    return "cards";
  }
}
