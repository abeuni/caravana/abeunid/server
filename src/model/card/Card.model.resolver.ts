import { Ctx, FieldResolver, Resolver, Root } from "type-graphql";

import { IRequestContext } from "@/middleware/context";
import { Event, EventEntityType, EventType } from "@/model/event/Event.model";
import { Person } from "../person/Person.model";
import { Card } from "./Card.model";

@Resolver((of) => Card)
export class CardResolver {
  @FieldResolver()
  public async events(
    @Ctx() ctx: IRequestContext,
    @Root() card: Card,
  ) {
    return Event.find(ctx, {
      relevant: card.id,
    });
  }

  @FieldResolver()
  public async person(
    @Ctx() ctx: IRequestContext,
    @Root() card: Card,
  ) {
    return card.getPerson(ctx, card.id);
  }
}
