import { Indexable } from "@/model/Indexable";

import { Field, ObjectType } from "type-graphql";

import { FormularyPage } from "@/model/page/FormularyPage.model";
import { Person } from "@/model/person/Person.model";
import { PersonCreateInput } from "@/model/person/PersonCreateInput.model";

@ObjectType({ description: "Represents a Physical Formulary (Set of pages)" })
export class PhysicalFormulary extends Indexable<PhysicalFormulary> {
  @Field({ description: "QR Code of SEC page" })
  public baseQR: Date;

  @Field((type) => [FormularyPage], {
    description: "Pages belonging to this document",
  })
  public pages: FormularyPage[];

  @Field((type) => Person, { description: "Person associated with formulary" })
  public person?: Person;
  public personId?: string;

  // required
  public getCollection(): string {
    return "physical_formularies";
  }
}
