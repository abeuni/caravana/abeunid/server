import { Ctx, FieldResolver, Resolver, Root } from "type-graphql";

import { IRequestContext } from "@/middleware/context";
import { Person } from "@/model/person/Person.model";
import { PhysicalFormulary } from "./PhysicalFormulary.model";

@Resolver((of) => PhysicalFormulary)
export class PhysicalFormularyResolver {
  @FieldResolver()
  public person(
    @Ctx() ctx: IRequestContext,
    @Root() formulary: PhysicalFormulary,
  ) {
    return Person.load(ctx, formulary.personId);
  }
}
