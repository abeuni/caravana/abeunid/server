import { Ctx, FieldResolver, Resolver, Root } from "type-graphql";

import { IRequestContext } from "@/middleware/context";
import { Card } from "@/model/card/Card.model";
import { Person } from "@/model/person/Person.model";
import { Room } from "../room/Room.model";
import { Station } from "../station/Station.model";
import { Event, EventEntityType } from "./Event.model";

@Resolver((of) => Event)
export class EventResolver {
  @FieldResolver()
  public async entity(
    @Ctx() ctx: IRequestContext,
    @Root() event: Event,
  ) {
    switch (event.entityType) {
      case EventEntityType.PERSON:
        return Person.load(ctx, event.entityId);
      case EventEntityType.CARD:
        return Card.load(ctx, event.entityId);
      case EventEntityType.STATION:
        return Station.load(ctx, event.entityId);
      case EventEntityType.ROOM:
        return Room.load(ctx, event.entityId);
    }
  }
}
