import { Indexable } from "@/model/Indexable";

import { createUnionType, Field, ID, ObjectType, registerEnumType } from "type-graphql";
import { Card } from "../card/Card.model";
import { Person } from "../person/Person.model";
import { Room } from "../room/Room.model";
import { Station } from "../station/Station.model";
import { EventPersonAssociatedData } from "./data/EventPersonAssociatedData.model";
import { EventPersonUpdatedData } from "./data/EventPersonUpdatedData.model";
import { EventStationCardReadData } from "./data/EventStationCardReadData.model";

export enum EventType {
  ASSOCIATED = "ASSOCIATED",
  CARD_READ = "CARD_READ",
  CREATED = "CREATED",
  UPDATED = "UPDATED",
  MOVED = "MOVED",
  LEFT = "LEFT",
}

registerEnumType(EventType, { name: "EventType" });

export enum EventEntityType {
  PERSON = "PERSON",
  CARD = "CARD",
  ROOM = "ROOM",
  STATION = "STATION",
}

registerEnumType(EventEntityType, { name: "EventEntityType" });

export const EventEntity = createUnionType({
  name: "EventEntity",
  types: () => [Person, Card, Station, Room],
});

export const EventData = createUnionType({
  name: "EventData",
  resolveType: (value: any) => {
    if (value) {
      if (value.type === "EventStationCardReadData") { return EventStationCardReadData; }
      if (value.type === "EventPersonAssociatedData") { return EventPersonAssociatedData; }
      if (value.type === "EventPersonUpdatedData") { return EventPersonUpdatedData; }
    }
  },
  types: () => [EventPersonAssociatedData, EventStationCardReadData, EventPersonUpdatedData],
});

@ObjectType({ description: "Represents a Event that happened to a event" })
export class Event extends Indexable<Event> {
  @Field((type) => EventEntityType, { description: "Event Entity Type" })
  public entityType: EventEntityType;

  @Field({ description: "Event Type" })
  public type: EventType;

  @Field((type) => [ID], { description: "All entities this event is relevant to" })
  public relevant: string[];

  @Field((type) => EventEntity, { description: "Entity associated with the event" })
  public entity: typeof EventEntity;
  public entityId: string;

  @Field((type) => EventData, { description: "Entity associated with the event", nullable: true })
  public data?: typeof EventData;

  // required
  public getCollection(): string {
    return "events";
  }
}
