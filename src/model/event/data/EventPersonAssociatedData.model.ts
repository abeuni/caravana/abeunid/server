import { Card } from "@/model/card/Card.model";
import { Field, ObjectType } from "type-graphql";

@ObjectType({ description: "Represents the data for a Person Event of type ASSOCIATED" })
export class EventPersonAssociatedData {
  @Field((type) => Card, { description: "Card" })
  public card: Card;
  public cardId: string;
}
