import { Ctx, FieldResolver, Resolver, Root } from "type-graphql";

import { IRequestContext } from "@/middleware/context";
import { Card } from "@/model/card/Card.model";
import { EventPersonAssociatedData } from "./EventPersonAssociatedData.model";

@Resolver((of) => EventPersonAssociatedData)
export class EventPersonAssociatedDataResolver {
  @FieldResolver()
  public card(
    @Ctx() ctx: IRequestContext,
    @Root() data: EventPersonAssociatedData,
  ) {
    return Card.load(ctx, data.cardId);
  }
}
