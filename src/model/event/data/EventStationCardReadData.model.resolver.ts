import { Ctx, FieldResolver, Resolver, Root } from "type-graphql";

import { IRequestContext } from "@/middleware/context";
import { Card } from "@/model/card/Card.model";
import { Person } from "@/model/person/Person.model";
import { Room } from "@/model/room/Room.model";
import { EventStationCardReadData } from "./EventStationCardReadData.model";

@Resolver((of) => EventStationCardReadData)
export class EventStationCardReadDataResolver {
  @FieldResolver()
  public person(
    @Ctx() ctx: IRequestContext,
    @Root() data: EventStationCardReadData,
  ) {
    return Person.load(ctx, data.personId);
  }

  @FieldResolver()
  public card(
    @Ctx() ctx: IRequestContext,
    @Root() data: EventStationCardReadData,
  ) {
    return Card.load(ctx, data.cardId);
  }

  @FieldResolver()
  public room(
    @Ctx() ctx: IRequestContext,
    @Root() data: EventStationCardReadData,
  ) {
    return data.roomId && Room.load(ctx, data.roomId);
  }
}
