import { PersonUpdateChanges } from "@/model/person/PersonUpdateChanges.model";
import { Field, ObjectType } from "type-graphql";

@ObjectType({ description: "Represents the data for a Person Event of type UPDATED" })
export class EventPersonUpdatedData {
  @Field((type) => PersonUpdateChanges, { description: "Changes made to person" })
  public updates: PersonUpdateChanges;
}
