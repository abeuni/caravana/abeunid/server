import { Card } from "@/model/card/Card.model";
import { Person } from "@/model/person/Person.model";
import { Room } from "@/model/room/Room.model";
import { Field, ObjectType } from "type-graphql";

@ObjectType({ description: "Represents the data for a Person Event of type LOCATION" })
export class EventStationCardReadData {
  @Field((type) => Card, { description: "Card" })
  public card: Card;
  public cardId: string;

  @Field((type) => Person, { description: "Person", nullable: true })
  public person?: Card;
  public personId?: string;

  @Field((type) => Room, { description: "Room", nullable: true })
  public room?: Card;
  public roomId?: string;
}
