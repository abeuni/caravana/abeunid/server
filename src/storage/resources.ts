import { storage } from "./storage";

export const resourcesBucket = storage.bucket(process.env.STORAGE_BUCKET_RESOURCES || "abeuni-resources");
