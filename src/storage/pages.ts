import { storage } from "./storage";

export const pagesBucket = storage.bucket(process.env.STORAGE_BUCKET_PAGES || "abeuni-pages");
