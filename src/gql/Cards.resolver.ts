import { Arg, Authorized, Ctx, ID, Query, Resolver, Subscription } from "type-graphql";

import { IRequestContext } from "@/middleware/context";

// Model
import { CardsMessageType, ICardsMessageTopic } from "@/messages/Cards.messages";
import { Card } from "@/model/card/Card.model";
import { ApolloError } from "apollo-server";

@Resolver()
export class CardsResolver {
  @Authorized()
  @Query((returns) => Card, {
    description: "Retrieves card with given ID",
  })
  public async card(@Ctx() ctx: IRequestContext, @Arg("id", (type) => ID) id: string) {
    const card = await Card.load(ctx, id);
    if (!card) {
      throw new ApolloError("Could not find Card", "NOT_FOUND");
    }
    return card;
  }

  @Authorized()
  @Query((returns) => [Card], {
    description: "Retrieves list of filtered cards",
  })
  public async cards(
    @Ctx() ctx: IRequestContext,
  ) {
    const cards = await Card.find(ctx, {});
    cards.reverse();
    return cards;
  }

  @Authorized()
  @Subscription((returns) => [Card], {
    description: "Listens for creation and deletion of cards",
    filter: ({ payload }) => [CardsMessageType.CREATED, CardsMessageType.DELETED].includes(payload.type),
    topics: ICardsMessageTopic,
  })
  public async watchCards(
    @Ctx() ctx: IRequestContext,
  ) {
    const cards = await Card.find(ctx, {});
    cards.reverse();
    return cards;
  }

  @Authorized()
  @Subscription((returns) => Card, {
    description: "Listens for updates of cards",
    filter: ({ payload, args }) => [CardsMessageType.UPDATED].includes(payload.type) && args.id === payload.card.id,
    topics: ICardsMessageTopic,
  })
  public async watchCard(
    @Ctx() ctx: IRequestContext,
    @Arg("id", (type) => ID) id: string,
  ) {
    return Card.load(ctx, id);
  }
}
