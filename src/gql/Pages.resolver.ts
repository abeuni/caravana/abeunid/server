import { Arg, Ctx, ID, Mutation, Query, Resolver } from "type-graphql";

import { IRequestContext } from "@/middleware/context";

// Model
import { FormularyPage } from "@/model/page/FormularyPage.model";
import { Person } from "@/model/person/Person.model";
import { PersonCreateInput } from "@/model/person/PersonCreateInput.model";
import { ApolloError } from "apollo-server";

@Resolver()
export class PagesResolver {
  @Query((returns) => FormularyPage, {
    description: "Retrieves person with given ID",
  })
  public async page(@Ctx() ctx: IRequestContext, @Arg("id", (type) => ID) id: string) {
    const page = await FormularyPage.load(ctx, id);
    if (!page) {
      throw new ApolloError("Could not find Page", "NOT_FOUND");
    }
    return page;
  }

  @Query((returns) => [FormularyPage], {
    description: "Retrieves list of filtered greetings",
  })
  public async pages(
    @Ctx() ctx: IRequestContext,
    @Arg("id", (type) => ID, { description: "Formulary ID", nullable: true })
    id?: string,
  ) {
    return Person.find(ctx, {});
  }

  @Mutation((returns) => Person, {
    description: "Uploads new formulary page image",
  })
  public uploadPage(
    @Ctx() ctx: IRequestContext,
    @Arg("data") data: PersonCreateInput,
  ) {
    return Person.create(ctx, data);
  }
}
