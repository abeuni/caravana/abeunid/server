import { Arg, Ctx, Query, Resolver } from "type-graphql";

import { IRequestContext } from "@/middleware/context";
import { CEPSearchResult } from "@/model/util/CEPSearchResult";
import axios from "axios";

@Resolver()
export class UtilResolver {
  @Query((type) => [CEPSearchResult], {
    description: "Searches for street names",
  })
  public async streetSearch(@Ctx() ctx: IRequestContext, @Arg("search") search: string) {
    const response = await axios.get(encodeURI(`https://viacep.com.br/ws/SP/São Paulo/${search}/json/`));

    return response.data.map((ret) => ({
      cep: ret.cep,
      neighborhood: ret.bairro,
      street: ret.logradouro,
    }));
  }
}
