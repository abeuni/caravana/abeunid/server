import { Arg, Ctx, ID, Mutation, Query, Resolver } from "type-graphql";

import { IRequestContext } from "@/middleware/context";

// Model
import { Event } from "@/model/event/Event.model";
import { Person } from "@/model/person/Person.model";
import { PersonCreateInput } from "@/model/person/PersonCreateInput.model";

@Resolver()
export class EventsResolver {
  @Query((returns) => Event, {
    description: "Retrieves person with given ID",
  })
  public Event(@Ctx() ctx: IRequestContext, @Arg("id", (type) => ID) id: string) {
    return Person.load(ctx, id);
  }

  @Query((returns) => [Event], {
    description: "Retrieves list of filtered greetings",
  })
  public async Events(
    @Ctx() ctx: IRequestContext,
    @Arg("id", (type) => ID, { description: "Formulary ID", nullable: true })
    id?: string,
  ) {
    return Person.find(ctx, {});
  }

  @Mutation((returns) => Person, {
    description: "Creates a new greeting",
  })
  public createPerson(
    @Ctx() ctx: IRequestContext,
    @Arg("data") data: PersonCreateInput,
  ) {
    return Person.create(ctx, data);
  }

}
