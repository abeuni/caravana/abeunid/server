import {
  Arg, Authorized, Ctx, ID, Int, Mutation, Publisher, PubSub, Query, Resolver, Subscription,
} from "type-graphql";

import { IRequestContext } from "@/middleware/context";

// Model
import { CardsMessageType, ICardsMessage, ICardsMessageTopic } from "@/messages/Cards.messages";
import { IPersonsMessage, IPersonsMessageTopic, PersonsMessageType } from "@/messages/Persons.messages";
import { Card } from "@/model/card/Card.model";
import { Event, EventEntityType, EventType } from "@/model/event/Event.model";
import { Person } from "@/model/person/Person.model";
import { PersonCreateInput } from "@/model/person/PersonCreateInput.model";
import { PersonFilterInput } from "@/model/person/PersonFilterInput.model";
import { PersonUpdateInput } from "@/model/person/PersonUpdateInput.model";
import { ApolloError } from "apollo-server";

// Makes a mongo filter from filter input
function personsFilter(filter: PersonFilterInput) {
  const mongoFilter: any = {};

  if (filter && filter.search) {
    mongoFilter.$text = { $search: filter.search };
  }

  return mongoFilter;
}

@Resolver()
export class PersonsResolver {
  @Authorized()
  @Query((returns) => Person, {
    description: "Retrieves person with given ID",
  })
  public async person(@Ctx() ctx: IRequestContext, @Arg("id", (type) => ID) id: string) {
    const person = await Person.load(ctx, id);
    if (!person) {
      throw new ApolloError("Could not find Person", "NOT_FOUND");
    }
    return person;
  }

  @Authorized()
  @Query((returns) => [Person], {
    description: "Retrieves list of filtered persons",
  })
  public async persons(
    @Ctx() ctx: IRequestContext,
    @Arg("filter", (type) => PersonFilterInput, { nullable: true }) filter: PersonFilterInput,
    @Arg("start", (type) => Int, { nullable: true, defaultValue: 0 }) start: number,
    @Arg("limit", (type) => Int, { nullable: true, defaultValue: 10 }) limit: number,
  ) {
    const persons = await Person.find(ctx, personsFilter(filter), (cursor) =>
      cursor.sort("created", -1).skip(start).limit(limit),
    );

    return persons;
  }

  @Authorized()
  @Mutation((returns) => Person, {
    description: "Creates a new person",
  })
  public async createPerson(
    @Ctx() ctx: IRequestContext,
    @Arg("data") data: PersonCreateInput,
    @PubSub(IPersonsMessageTopic) personsPublisher: Publisher<IPersonsMessage>,
  ) {

    if (data.document && (await Person.find(ctx, {
      "document.type": data.document.type,
      "document.value": data.document.value,
    })).length > 0) {
      throw new ApolloError("Person with given document already exists", "209");
    }

    const person = await Person.create(ctx, data);
    await Event.create(ctx, {
      entityId: person.id,
      entityType: EventEntityType.PERSON,
      relevant: [person.id],
      type: EventType.CREATED,
    });

    await personsPublisher({ type: PersonsMessageType.CREATED, person });

    return person;
  }

  @Authorized()
  @Mutation((returns) => Person, {
    description: "Updates a person",
  })
  public async updatePerson(
    @Ctx() ctx: IRequestContext,
    @Arg("id", (type) => ID) id: string,
    @Arg("data") data: PersonUpdateInput,
    @PubSub(IPersonsMessageTopic) personsPublisher: Publisher<IPersonsMessage>,
  ) {

    const person = await Person.load(ctx, id);

    await person.update(ctx, data);

    await Event.create(ctx, {
      data: {
        type: "EventPersonUpdatedData",
        updates: data,
      },
      entityId: person.id,
      entityType: EventEntityType.PERSON,
      relevant: [person.id],
      type: EventType.UPDATED,
    });

    await personsPublisher({ type: PersonsMessageType.UPDATED, person });

    return person;
  }

  @Authorized()
  @Mutation((returns) => Card, {
    description: "Assigns a card to a person",
  })
  public async associateCard(
    @Ctx() ctx: IRequestContext,
    @Arg("personId", (type) => ID) personId: string,
    @Arg("code") code: string,
    @PubSub(IPersonsMessageTopic) personsPublisher: Publisher<IPersonsMessage>,
    @PubSub(ICardsMessageTopic) cardsPublisher: Publisher<ICardsMessage>,
  ) {

    const person = await Person.load(ctx, personId);
    const cards = await Card.find(ctx, {
      code,
    });

    let card;

    // Create card if not exists
    if (cards.length < 1) {
      card = await Card.create(ctx, {
        code,
      });

      await Event.create(ctx, {
        entityId: card.id,
        entityType: EventEntityType.CARD,
        relevant: [card.id],
        type: EventType.CREATED,
      });

      await cardsPublisher({ type: CardsMessageType.CREATED, card });
    } else {
      card = cards[0];
    }

    // Create events
    await Event.create(ctx, {
      data: {
        cardId: card.id,
        type: "EventPersonAssociatedData",
      },
      entityId: person.id,
      entityType: EventEntityType.PERSON,
      relevant: [person.id, card.id],
      type: EventType.ASSOCIATED,
    });

    // Send updates
    await cardsPublisher({ type: CardsMessageType.UPDATED, card });
    await personsPublisher({ type: PersonsMessageType.UPDATED, person });

    return card;
  }

  @Authorized()
  @Subscription((returns) => [Person], {
    description: "Listens for creation and deletion of persons",
    filter: ({ payload }) => [PersonsMessageType.CREATED, PersonsMessageType.DELETED].includes(payload.type),
    topics: IPersonsMessageTopic,
  })
  public async watchPersons(
    @Ctx() ctx: IRequestContext,
    @Arg("filter", (type) => PersonFilterInput, { nullable: true }) filter: PersonFilterInput,
    @Arg("start", (type) => Int, { nullable: true, defaultValue: 0 }) start: number,
    @Arg("limit", (type) => Int, { nullable: true, defaultValue: 10 }) limit: number,
  ) {
    const persons = await Person.find(ctx, personsFilter(filter), (cursor) =>
      cursor.sort("created", -1).skip(start).limit(limit),
    );
    return persons;
  }

  @Authorized()
  @Subscription((returns) => Person, {
    description: "Listens for updates of persons",
    filter: ({ payload, args }) => [PersonsMessageType.UPDATED].includes(payload.type) && args.id === payload.person.id,
    topics: IPersonsMessageTopic,
  })
  public async watchPerson(
    @Ctx() ctx: IRequestContext,
    @Arg("id", (type) => ID) id: string,
  ) {
    return Person.load(ctx, id);
  }
}
