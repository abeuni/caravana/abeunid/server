import { Arg, Ctx, ID, Mutation, Query, Resolver } from "type-graphql";

import { IRequestContext } from "@/middleware/context";

// Model
import { Caravan } from "@/model/caravan/Caravan.model";
import { CaravanCreateInput } from "@/model/caravan/CaravanCreateInput.model";

@Resolver()
export class CaravansResolver {
  @Query((returns) => Caravan, {
    description: "Retrieves caravan with given ID",
  })
  public caravan(@Ctx() ctx: IRequestContext, @Arg("id", (type) => ID) id: string) {
    return Caravan.load(ctx, id);
  }

  @Query((returns) => [Caravan], {
    description: "Retrieves list of filtered caravans",
  })
  public caravans(
    @Ctx() ctx: IRequestContext,
  ) {
    return Caravan.find(ctx, {});
  }

  @Mutation((returns) => Caravan, {
    description: "Creates a new caravan",
  })
  public async createCaravan(
    @Ctx() ctx: IRequestContext,
    @Arg("data") data: CaravanCreateInput,
  ) {
    return Caravan.create(ctx, data);
  }
}
