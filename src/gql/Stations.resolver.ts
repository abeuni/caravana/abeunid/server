import { Arg, Ctx, ID, Int, Mutation, Publisher, PubSub, Query, Resolver, Subscription } from "type-graphql";

import { IRequestContext } from "@/middleware/context";

// Model
import { CardsMessageType, ICardsMessage, ICardsMessageTopic } from "@/messages/Cards.messages";
import { IRoomsMessage, IRoomsMessageTopic, RoomsMessageType } from "@/messages/Rooms.messages";
import { IStationsMessage, IStationsMessageTopic, StationsMessageType } from "@/messages/Stations.messages";
import { Card } from "@/model/card/Card.model";
import { Event, EventEntityType, EventType } from "@/model/event/Event.model";
import { Room } from "@/model/room/Room.model";
import { Station } from "@/model/station/Station.model";
import { ApolloError } from "apollo-server";

@Resolver()
export class StationsResolver {
  @Query((returns) => Station, {
    description: "Retrieves station with given ID",
  })
  public async station (@Ctx() ctx: IRequestContext, @Arg("id", (type) => ID) id: string) {
    const station = await Station.load(ctx, id);

    if (!station) {
      throw new ApolloError("Could not find Station", "NOT_FOUND");
    }

    return station;
  }

  @Query((returns) => [Station], {
    description: "Retrieves list of filtered stations",
  })
  public async stations (
    @Ctx() ctx: IRequestContext,
  ) {
    const stations = await Station.find(ctx, {});
    stations.reverse();
    return stations;
  }

  @Mutation((returns) => Station, { nullable: true })
  public async scannedCard (
    @Ctx() ctx: IRequestContext,
    @PubSub(ICardsMessageTopic) cardsPublisher: Publisher<ICardsMessage>,
    @PubSub(IStationsMessageTopic) stationsPublisher: Publisher<IStationsMessage>,
    @PubSub(IRoomsMessageTopic) roomsPublisher: Publisher<IRoomsMessage>,
    @Arg("node", (type) => Int, { description: "Node that scanned the card" }) node: number,
    @Arg("nuid", { description: "NUID of the read card", nullable: true }) nuid?: string,
  ) {
    // Get station
    const stationSearchResult: (Station | Error)[] = await Station.find(ctx, { node });

    let station: Station = null;

    if (stationSearchResult[0] instanceof Error)
      throw new ApolloError('Unknown error!')

    // Create station if not exists
    if (stationSearchResult && stationSearchResult.length > 0) {
      station = stationSearchResult[0];
    } else {
      station = await Station.create(ctx, {
        name: String(node),
        node,
      });

      await Event.create(ctx, {
        entityId: station.id,
        entityType: EventEntityType.STATION,
        relevant: [station.id],
        type: EventType.CREATED,
      });

      await stationsPublisher({ type: StationsMessageType.CREATED, station });
    }

    // If NUID is null, only create station
    if (!nuid) { return null; }

    // Get room from station
    const room: Room = station.roomId && await Room.load(ctx, station.roomId);

    // Get card
    const cardSearchResult: (Card | Error)[] = await Card.find(ctx, { code: nuid });

    let card: Card = null;

    if (cardSearchResult[0] instanceof Error)
      throw new ApolloError('Unknown error!')

    // Create card if not exists
    if (cardSearchResult && cardSearchResult.length > 0) {
      card = cardSearchResult[0];
    } else {
      card = await Card.create(ctx, {
        code: nuid,
      });

      await Event.create(ctx, {
        entityId: card.id,
        entityType: EventEntityType.CARD,
        relevant: [card.id],
        type: EventType.CREATED,
      });

      await cardsPublisher({ type: CardsMessageType.CREATED, card });
    }

    await Event.create(ctx, {
      data: {
        cardId: card.id,
        personId: null, // TODO: ADD PERSON ID TO DATA
        roomId: station.roomId,
        type: "EventStationCardReadData",
      },
      entityId: station.id,
      entityType: EventEntityType.STATION,
      relevant: [station.id, card.id, ...(station.roomId ? [station.roomId] : [])],
      type: EventType.CARD_READ,
    });

    await stationsPublisher({ type: StationsMessageType.UPDATED, station });
    await cardsPublisher({ type: CardsMessageType.UPDATED, card });
    if (room) { await roomsPublisher({ type: RoomsMessageType.UPDATED, room }); }

    return station;
  }

  @Subscription((returns) => [Station], {
    description: "Listens for creation and deletion of stations",
    filter: ({ payload }) => [StationsMessageType.CREATED, StationsMessageType.DELETED].includes(payload.type),
    topics: IStationsMessageTopic,
  })
  public async watchStations (
    @Ctx() ctx: IRequestContext,
  ) {
    const stations = await Station.find(ctx, {});
    stations.reverse();
    return stations;
  }

  @Subscription((returns) => Station, {
    description: "Listens for updates of stations",
    filter: ({ payload, args }) => {
      return [StationsMessageType.UPDATED].includes(payload.type) && args.id === payload.station.id;
    },
    topics: IStationsMessageTopic,
  })
  public async watchStation (
    @Ctx() ctx: IRequestContext,
    @Arg("id", (type) => ID) id: string,
  ) {
    return Station.load(ctx, id);
  }
}
